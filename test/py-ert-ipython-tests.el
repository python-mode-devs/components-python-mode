;; py-ert-ipython-tests.el --- testing python-mode.el -*- lexical-binding: t; -*-

;; Keywords: languages

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'py-setup-ert-tests)

;;;

(ert-deftest py-ert-ipython-lp-1398530-test-7re3GL ()
  (py-test-with-temp-buffer
      ""
    (when (buffer-live-p (get-buffer "*IPython*"))(py-kill-buffer-unconditional "*IPython*"))
    (if (executable-find "ipython")
        (let ((py-shell-name "ipython"))
          (py-shell)
          (sit-for 0.1 t)
          (should (buffer-live-p (get-buffer "*IPython*"))))
      (when py-debug-p (message "%s" "Seem no ‘ipython’ at this system.")))))

(ert-deftest py-ipython-shell-test-nqjTml ()
  ""
  (if (not (executable-find "ipython"))
      (message "py-ipython-shell-test-nqjTml: %s" "No executable found!")
    (let ((erg (ipython)))
      (sit-for 1)
      (should (bufferp (get-buffer erg)))
      (should (get-buffer-process erg)))))

(provide 'py-ert-ipython-tests)
;;; py-ert-ipython-tests.el ends here
