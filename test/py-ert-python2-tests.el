;;; py-ert-python2-tests.el --- py-execute-region tests

;; URL: https://gitlab.com/python-mode-devs
;; Keywords: languages, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'py-setup-ert-tests)

(ert-deftest py-ert-execute-region-python2-test ()
  (py-test-with-temp-buffer
      "print(\"one\")
print(\"two\")"
    (let ((buffer (py--choose-buffer-name "python2")))
      (py-execute-region-python2 (point-min) (point-max))
      ;; (sit-for 0.5 t)
      (set-buffer buffer)
      (goto-char (point-max))

      (should (search-backward "two")))))

(ert-deftest py-ert-execute-region-ipython3-test ()
  (py-test-with-temp-buffer
      "print(\"one\")
print(\"two\")"
    (let ((buffer (py--choose-buffer-name "ipython3"))
	  (inhibit-point-motion-hooks t))
      (py-execute-region-ipython3 (point-min) (point-max))
      (set-buffer buffer)
      (accept-process-output (get-buffer-process buffer) 0.1)
      ;; (goto-char (point-max))
      (switch-to-buffer (current-buffer))
      (goto-char (point-max))
      (sit-for 0.5 t)
      (should (search-backward "two")))))

(ert-deftest py-ert-execute-region-jython-test ()
  (py-test-with-temp-buffer
      "print(\"one\")
print(\"two\")"
    (let ((buffer (py--choose-buffer-name "jython")))
      (py-execute-region-jython (point-min) (point-max))
      (set-buffer buffer)
      (switch-to-buffer (current-buffer))
      (goto-char (point-max))
      (sit-for 0.5 t)
      (should (search-backward "two")))))

(ert-deftest py-ert-always-split-dedicated-lp-1361531-python2-test-DuoIzY ()
  (py-test-with-temp-buffer
      "#! /usr/bin/env python2
# -*- coding: utf-8 -*-
print(\"I'm the py-always-split-dedicated-lp-1361531-python2-test\")"
    (delete-other-windows)
    (let* ((py-split-window-on-execute 'always)
	   (erg1 (progn (py-execute-statement-python2-dedicated) py-output-buffer))
	   (erg2 (progn (py-execute-statement-python2-dedicated) py-output-buffer)))
      (sit-for 1 t)
      (when py-debug-p (message "(count-windows) %s" (count-windows)))
      (should (< 2 (count-windows)))
      (py-kill-buffer-unconditional erg1)
      (py-kill-buffer-unconditional erg2)
      (py-restore-window-configuration))))

(ert-deftest py-ert-execute-region-python2-test ()
  (py-test-with-temp-buffer
      "print(\"one\")
print(\"two\")"
    (let ((buffer (py--choose-buffer-name "python2")))
      (py-execute-region-python2 (point-min) (point-max))
      ;; (sit-for 0.5 t)
      (set-buffer buffer)
      (goto-char (point-max))

      (should (search-backward "two")))))

(ert-deftest py-ert-execute-region-python2-test-XOPWqH ()
  (py-test-with-temp-buffer
      "print(\"I'm the py-ert-execute-region-python2-test\")"
    (let (py-result
	  (py-store-result-p t))
      (goto-char (point-max))
      (push-mark)
      (goto-char (point-min))
      (py-execute-region-python2 (region-beginning) (region-end))
      (sit-for 0.1 t)
      (should (string-match "py-ert-execute-region-python2-test" py-result)))))

(ert-deftest py-ert-execute-statement-python2-fast-1-7RNU3c ()
  (py-test-with-temp-buffer-point-min
   "print(1)"
   (let ((py-fast-process-p t)
	 (py-return-result-p t)
	 py-result
	 (py-store-result-p t))
     (py-execute-statement "python2")
     (should (string= "1" py-result)))))

(ert-deftest py-ert-execute-statement-python2-test-Z1bsHy ()
  (py-test-with-temp-buffer-point-min
      "print(\"I'm the py-execute-statement-python2-test\")"
    (when py-debug-p (switch-to-buffer (current-buffer))
	  (jit-lock-fontify-now))
    (py-execute-statement-python2)
    (set-buffer "*Python2*")
    (goto-char (point-max))
    (sit-for 0.2 t)
    (and (should (search-backward "py-execute-statement-python2-test" nil t 1))
	 (py-kill-buffer-unconditional (current-buffer)))))

(ert-deftest py-ert-just-two-split-dedicated-lp-1361531-python2-test ()
  (py-test-with-temp-buffer
      "#! /usr/bin/env python2
# -*- coding: utf-8 -*-
print(\"I'm the py-just-two-split-dedicated-lp-1361531-python2-test\")"
    (delete-other-windows)
    (let* ((py-split-window-on-execute 'just-two)
	   (erg1 (progn (py-execute-statement-python2-dedicated) py-output-buffer))
	   (erg2 (progn (py-execute-statement-python2-dedicated) py-output-buffer)))
      ;; (sit-for 1 t)
      (when py-debug-p (message "(count-windows) %s" (count-windows)))
      (should (eq 2 (count-windows)))
      (py-kill-buffer-unconditional erg1)
      (py-kill-buffer-unconditional erg2)
      (py-restore-window-configuration))))

(ert-deftest py-face-lp-1454858-python2-1-test-cBoEWe ()
  (py-test-with-temp-buffer
      "#! /usr/bin/env python2
file.close()"
    (goto-char(point-max))
    (let ((py-python-edit-version ""))
      (font-lock-fontify-region (point-min) (point-max))
      (forward-line 1)
      (should (eq (face-at-point) 'py-builtins-face)))))

(ert-deftest py-face-lp-1454858-python2-3-test-X7oyjk ()
  (py-test-with-temp-buffer
      "#! /usr/bin/env python2
print()"
    (goto-char(point-max))
    (let ((py-python-edit-version ""))
      (font-lock-fontify-region (point-min) (point-max))
      (forward-line 1)
      (should (eq (face-at-point) 'font-lock-keyword-face)))))

(ert-deftest py-face-lp-1454858-python3-3-test-xPEpuc ()
  (let ((py-python-edit-version "python3"))
    (py-test-with-temp-buffer
	"#! /usr/bin/env python2
print()"
      (goto-char (point-max))
      (beginning-of-line)
      (sit-for 0.1)
      (should (eq (face-at-point) 'py-builtins-face)))))

(ert-deftest py-shell-python2-lp-1398530-test-TaiABe ()
  (when (buffer-live-p (get-buffer "*Python2*"))(py-kill-buffer-unconditional "*Python2*"))
  (py-test-with-temp-buffer
      ""
    (when py-debug-p (switch-to-buffer (current-buffer)))
    (let ((py-shell-name "python2"))
      (py-shell)
      (sit-for 0.1 t)
      (should (buffer-live-p (get-buffer "*Python2*"))))))

(ert-deftest py-complete-in-python-shell-test-LYnVJh ()
  (ignore-errors (py-kill-buffer-unconditional "*Python*"))
  (ignore-errors (py-kill-buffer-unconditional "*Python3*"))
  (set-buffer (python))
  (goto-char (point-max))
  (insert "pri")
  (py-indent-or-complete)
  (sit-for 0.1)
  (should (or (eq ?t (char-before))(eq ?\( (char-before)))))

(provide 'py-ert-python2-tests)
;;; py-ert-python2-tests.el here
