;; py-ert-ipython3-tests.el --- testing python-mode.el -*- lexical-binding: t; -*-

;; Keywords: languages

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'py-setup-ert-tests)

;;;


(ert-deftest py-honor-py-python-command-7JbtYW ()
  (py-test-with-temp-buffer
      "print(123)"
    (let ((py-python-command "ipython3")
	  (py-return-result-p t))
      (py-execute-buffer)
      (should (buffer-live-p (get-buffer  "*IPython3*")))
      (should (string= "123" py-result)))))

(ert-deftest py-honor-py-python-command-kroygP ()
  (py-test-with-temp-buffer
      "print(123)"
    (let ((py-python-command "ipython3"))
      (py-shell))))


(provide 'py-ert-ipython3-tests)
;;; py-ert-ipython3-tests.el ends here
